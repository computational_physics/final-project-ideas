# Final project ideas

In this repository you can find a collection of final project possibilities for Computational Physics. Each of the folders here corresponds to a possible project and contains a number of original references.

Below you can find a short description of the projects, categorized roughly in
terms of complexity

### *medium*

- [`LatticeBoltzmann`](LatticeBoltzmann):
  Simulate fluid dynamics (including e.g. turbulence) using a lattice-based description. Instead of solving the Navier-Stokes equation directly (which is hard), an approximation of fluid flow on a lattice is implemented.
- [`MarchOfThePenguins`](MarchOfThePenguins):
  Simulate a soft-matter system inspired by the dynamics of groups of pinguins in Antarctica. This is part of a larger class of simulations where the behavior of large groups of animals is simulated by having individual animals behave according to simple rules. Together, they give rise to a more complex collective behavior.
- [`Piano`](Piano):
  Simulate a piano by simulating the vibrations of real string hit by a hammer. By solving a differential equation for a piano string using parameters corresponding to a real piano you can experience how such an instrument really works. When you connect the simulated wave to speakers, you can hear your own virtual piano playing.
- [`QuantumWavePropagation`](QuantumWavePropagation):
  Simulate the propagation of quantummechanical wave packets. Use this to visualize the quantum effects that you know from the quantum mechanics class and
  explore tunneling and interference!
- [`QuantumTrajectories`](QuantumTrajectories):
  Simulate open quantum systems, where interactions with an environment affect the dynamics (the environment "measures" the quantum system). You can simulate this using quantum trajectories and quantum jumps.
- [`SelfOrganizedCriticality`](SelfOrganizedCriticality):
  Simulate the patterns created by complex processes in nature, such as piling up sand. Explore the physical behavior of criticality that exhibits power-laws and fractality.

### *hard*

- [`SmoothParticleHydrodynamics`](SmoothParticleHydrodynamics):
  Simulate the behavior of fluids using an approximate method that approximates the fluid with particles and interactions with smoothing kernels. With this technique, you can simulate for exampel water drops falling on the ground and splashing to all sides.
- [`LatticeBoltzmannWithObjects`](LatticeBoltzmannWithObjects):
  Simulate movable objects swimming in a fluid using a lattice-based description. This is an extension of the `LatticeBoltzmann` project.
  